#/bin/bash

if [ -f $JBOSS_HOME/.jboss_admin_pass_configured ]; then
    echo "JBoss admin user's password has been configured!"
    exit 0
fi

echo "=> Configuring jboss user in Wildfly"
$JBOSS_HOME/bin/add-user.sh --silent=true jboss ${JBOSS_PASS}
echo "=> Done!"
echo "========================================================================"
echo "You can now configure to this JBoss server using:"
echo ""
echo "    jboss:${JBOSS_PASS}"
echo ""
echo "========================================================================"

touch $JBOSS_HOME/.jboss_admin_pass_configured
