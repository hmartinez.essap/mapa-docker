#!/bin/bash
# USO:
# Agregar al crontab:
#  0 0,12 * * * sh /opt/mapa_docker/script_backup.sh
echo -e

# Location to place backups.
backup_dir="/opt/backups/db/"
#String to append to the name of the backup files
backup_date=`date +%d-%m-%Y`
#Numbers of days you want to keep copie of your databases
number_of_days=30
database_name="mawio"

echo [DOCKER] Dumping $database_name
docker exec mapadocker_db_1 pg_dump -h localhost -U postgres $database_name -f /tmp/backup.sql

echo [SERVER] Copy to $backup_dir$database_name\_$backup_date\.sql
touch $backup_dir$database_name\_$backup_date\.sql
docker cp mapadocker_db_1:/tmp/backup.sql $backup_dir$database_name\_$backup_date\.sql

